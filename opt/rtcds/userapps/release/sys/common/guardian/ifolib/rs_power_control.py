#!/usr/bin/env python

import time

##################################################

# define the needed channels for the three systems that use the same
# control logic: PSL, TCSX, TCSY
SYSTEM_CHANNELS = {
    'PSL': dict(
        POWER_IN_REF = 'IMC-PWR_EOM_OUTPUT',

        RS_POWER_IN = 'PSL-ROTATIONSTAGE_POWER_IN',
        RS_POWER_REQUEST = 'PSL-ROTATIONSTAGE_POWER_REQUEST',
        RS_EXEC_STATE = 'PSL-ROTATIONSTAGE_RS_EXECUTE',
        RS_ACTUATE = 'PSL-ROTATIONSTAGE_COMMAND',
        RS_ABORT = 'PSL-ROTATIONSTAGE_ABORT',

        POWER_OUT = 'IMC-PWR_IN_OUT16',
        ),
    }
for s in ['X', 'Y']:
    sys = 'TCS'+s
    SYSTEM_CHANNELS[sys] = dict(
        POWER_IN_REF = 'TCS-ITM'+s+'_CO2_LSRPWR_HD_PD_OUTPUT',

        RS_POWER_IN = 'TCS-C_CO2_'+s+'_LASERPOWER_POWER_IN',
        RS_POWER_REQUEST = 'TCS-C_CO2_'+s+'_LASERPOWER_POWER_REQUEST',
        RS_EXEC_STATE = 'TCS-C_CO2_'+s+'_LASERPOWER_RS_EXECUTE',
        RS_ACTUATE = 'TCS-C_CO2_'+s+'_LASERPOWER_COMMAND',
        RS_ABORT = 'TCS-C_CO2_'+s+'_LASERPOWER_ABORT',

        POWER_OUT = 'TCS-ITM'+s+'_CO2_LSRPWR_MTR_OUT16',
        )

##################################################

class RotationStageError(Exception):
    pass


def rs_get_power(system):
    """Get the current output power.

    """
    sys = SYSTEM_CHANNELS[system]
    return ezca[sys['POWER_OUT']]


def rs_update_calc(system):
    """Update the power/angle calculation.

    Update the input power level for the RS angle calculation from the
    IN_REF.

    """
    sys = SYSTEM_CHANNELS[system]
    # FIXME: should we average this at all?  maybe read DECIMATE
    # output IN_REF?
    ezca[sys['RS_POWER_IN']] = ezca[sys['POWER_IN_REF']]


def rs_goto_power(system, power):
    """Go to power specified by POWER_REQUEST channel.

    """
    sys = SYSTEM_CHANNELS[system]

    # wait for RS action to complete if already executing
    t0 = time.time()
    while ezca[sys['RS_EXEC_STATE']] == 1:
	time.sleep(0.2)
	if time.time() > t0 + 60:
	    raise RotationStageError("Waited for 60 seconds for prior execution to complete, throwing error")

    # set RS power and actuate
    ezca[sys['RS_POWER_REQUEST']] = power
    time.sleep(0.1)
    ezca[sys['RS_ACTUATE']] = 2

    time.sleep(0.2)

    
    while ezca[sys['RS_EXEC_STATE']] == 1:
        time.sleep(0.2)
        if time.time() > t0 + 60:
            raise RotationStageError("did not complete rotation after 60 seconds")

    # wait another fraction of a second to make sure everything has
    # finalized.
    time.sleep(0.2)


def rs_bootstrap_power(system, power):
    # bootstrap to adjust the final power
    # assumes you're already very close to the requested power
    sys = SYSTEM_CHANNELS[system]
    power_current = ezca[sys['POWER_OUT']]
    power_adjusted = (power**2) / power_current
    ezca[sys['RS_POWER_REQUEST']] = power_adjusted
    ezca[sys['RS_ACTUATE']] = 2

##################################################

if __name__ == '__main__':
    import argparse

    description = """Actuate PSL or TCS power control rotation stage. 
"""

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('system', type=str,
                        choices=sorted(SYSTEM_CHANNELS.keys()),
                        help="system to actuate")
    parser.add_argument('power', type=float, nargs='?',
                        help="requested power")
    args = parser.parse_args()

    from ezca import Ezca

    Ezca().export()

    if args.power:
        rs_goto_power(args.system, args.power)
    else:
        print rs_get_power(args.system)
