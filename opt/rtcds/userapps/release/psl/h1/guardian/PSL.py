

from guardian import GuardState
import time
# These are located in userapps, /sys/common/guardian/
import ifolib.rs_power_control as rsp
from ifolib.ifo_power_control import IFOPowerStep


##################################################

nominal = 'POWER_23W'

# This is taken care of by IMC_LOCK in a dec
# gain in db for IMC servo during acquisition, set during power reset
#imc_servo_acquire_in1gain = -13#-26

# reset power in Watts
RESET_POWER = 2

# Default power to go to after lockloss
DEFAULT_POWER = 2

# list of requestable powers in Watts
REQUEST_POWERS = [2,10,23]

# Max power available on GRD
MAX_POWER = 25

##################################################


#### FIXME: Do this

class INIT(GuardState):
    request = True

    def main(self):
        pass



# Option 1: The LLO way
# This didnt work... not sure why
"""
def gen_goto_state(power, index, step_gains=True, step_refl_gain=True):
    class POWER(GuardState):
        goto = True
        request = False

        def main(self):
            p = power
            if ezca['PSL-POWER_REQUEST'] == p:
                self.done = True
                return True
            log("Moving IFO input power to %s watts..." % p)
            self.step = IFOPowerStep(p, step_gains=step_gains, step_refl_gain=step_refl_gain)
            self.final = False
            self.done = False

        def run(self):
            if self.done:
                return True
            elif self.final:
                if self.timer['settle']:
                    log("adjusting PSL-POWER_SCALE to final output power")
                    ezca['PSL-POWER_SCALE_OFFSET'] = rsp.rs_get_power('PSL')
                    self.done = True
                    return True
            elif self.step.step():
                # adjust the SCALE to the actual output power
                # FIXME: do we want to do this here, i.e. set the
                # SCALE to the actual output power, or do we want to
                # just leave it as the request?
                # wait for the power to settle
                settle = 10
                log("waiting for power to settle %s sec..." % settle)
                self.timer['settle'] = settle
                self.final = True
    POWER.index = index
    return POWER
"""

# Option 2: like in ISC_LOCK
# This also did not work...

def gen_goto_state(power, index, rs_velocity=50):
    class POWER(GuardState):
        goto = True
        request = False

        def main(self):
            ezca['PSL-ROTATIONSTAGE_RS_VELOCITY'] = rs_velocity


            # This doesn't work if the request is already that
            if ezca['PSL-POWER_REQUEST'] == power:
                return True


            log("Moving IFO input power to %s watts..." % power)
            #ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']            
            ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] = power
            # Needs this otherwise the button is pressed before the power is in
            time.sleep(0.1)
            # Press the GO_TO_POWER button
            ezca['PSL-ROTATIONSTAGE_COMMAND'] = 2
            time.sleep(1)

        def run(self):
            if ezca['PSL-ROTATIONSTAGE_RS_BUSY']:
                # Set the offset to the current power
                ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']
            else:
                if abs(ezca['IMC-PWR_IN_OUTMON'] - power) >= 2:
                    notify('Adjust the power by hand or search for home!')
                else:
                    return 'POWER_{}W'.format(power)
    POWER.index = index
    return POWER


# Option 3: using rs_goto_power()
# Worked but the pwr scaling was not done in run, seemed to do it right away
"""
def gen_goto_state(power, index, step_gains=True, step_refl_gain=True):
    class POWER(GuardState):
        goto = True
        request = False

        def main(self):
            log("Moving PSL rotation stage to %s watt..." % 1)
            ezca['PSL-POWER_REQUEST'] = power
            ezca['PSL-POWER_SCALE_OFFSET'] = power
            rsp.rs_goto_power('PSL', power)
        def run(self):
            if ezca['PSL-ROTATIONSTAGE_RS_BUSY']:
                # Set the offset to the current power
                ezca['PSL_POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']
            else:
                if abs(ezca['IMC-PWR_IN_OUTMON'] - power) >= 2:
                    notify('Adjust the power by hand or search for home!')
                else:
                    return True
    POWER.index = index
    return POWER
"""            
            

def gen_idle_state(index, requestable=False):
    class IDLE_STATE(GuardState):
        request = requestable
        def main(self):
            return True
        def run(self):
            if abs(ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] - ezca['IMC-PWR_IN_OUTMON']) >= 2:
                notify('Adjust the power by hand or search for home!')
            else:
                return True
    IDLE_STATE.index = index
    return IDLE_STATE



class POWER_RESET(GuardState):
    index = 1
    goto = True

    def main(self):
        log("Moving PSL rotation stage to %s watt..." % 1)
        rsp.rs_update_calc('PSL')
        ezca['PSL-POWER_REQUEST'] = RESET_POWER
        ezca['PSL-POWER_SCALE_OFFSET'] = RESET_POWER
        #ezca['IMC-REFL_SERVO_IN1GAIN'] = imc_servo_acquire_in1gain ### IMC_LOCK dec should take care of this on its own
        rsp.rs_goto_power('PSL', RESET_POWER)
        self.done = False
        #self.timer['wait'] = 10

    def run(self):
        return True


class SEARCHING_FOR_HOME(GuardState):
    """Hit the command through the epics channel and move to home

    """
    index = 4
    goto = True
    def main(self):
        log('Rot. stage moving to Home')
        # Give the search for home command
        ezca['PSL-ROTATIONSTAGE_COMMAND'] = 4
        ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']

    def run(self):
        if ezca['PSL-ROTATIONSTAGE_RS_BUSY']:
            # Set the offset to the current power
            ezca['PSL-POWER_SCALE_OFFSET'] = ezca['IMC-PWR_IN_OUTMON']
        else:
            return True


class HOME(GuardState):
    """Idle state to wait at home until a power is requested and not check
    that the poweer request is near the IMC output power (like gen_idle_state)
    """
    index = 5
    goto = False
    def main(self):
        return True
    def run(self):
        return True
##################################################
edges = []

edges += [('SEARCHING_FOR_HOME', 'HOME')]

# Slow to max power
globals()['GOTO_POWER_{}W_SLOW'.format(MAX_POWER)] = gen_goto_state(MAX_POWER, MAX_POWER * 10 - 3, rs_velocity=10)
globals()['POWER_{}W'.format(MAX_POWER)] = gen_idle_state(MAX_POWER * 10 -2, requestable=True)
edges += [('GOTO_POWER_{}W_SLOW'.format(MAX_POWER), 'POWER_{}W'.format(MAX_POWER))]
# Fast back to default power
globals()['GOTO_POWER_{}W_FAST'.format(DEFAULT_POWER)] = gen_goto_state(DEFAULT_POWER, DEFAULT_POWER * 10 - 6, rs_velocity=100)
globals()['POWER_{}W'.format(DEFAULT_POWER)] = gen_idle_state(DEFAULT_POWER * 10 -5, requestable=True)
edges += [('GOTO_POWER_{}W_FAST'.format(DEFAULT_POWER), 'POWER_{}W'.format(DEFAULT_POWER))]

#GOTO_POWER_1W_NO_REFL_SCALE = gen_goto_state(1, 9, step_refl_gain=False)
#POWER_1W_NO_REFL_SCALE = gen_idle_state(10)
#edges += [('GOTO_POWER_1W_NO_REFL_SCALE', 'POWER_1W_NO_REFL_SCALE')]



##################################################
# automatically generate basic power request states
# This should make the powers in the request list, requestable
# and then every integer power should be available as non-requestable
for i in range(1,MAX_POWER+1):
    index = i * 10
    state_goto = 'GOTO_POWER_{}W'.format(i)
    state_idle = 'POWER_{}W'.format(i)
    globals()[state_goto] = gen_goto_state(i, index-1)
    if i in REQUEST_POWERS:
        globals()[state_idle] = gen_idle_state(index, requestable=True)
    else:
        globals()[state_idle] = gen_idle_state(index)
    edges += [(state_goto, state_idle)]
        

##################################################
# SVN $Id$
# $HeadURL$
##################################################
